import { useState } from "react";

export interface Headers {
  title: string;
  description: string;
}

export const loadTableHeaders = (): Headers => {
  const savedHeaders = sessionStorage.getItem("tableHeaders");
  return savedHeaders
    ? JSON.parse(savedHeaders)
    : { title: "Title", description: "Description" };
};

export const saveTableHeaders = (headers: Headers) => {
  sessionStorage.setItem("tableHeaders", JSON.stringify(headers));
};

export const useEditingHeader = () => {
  return useState<"title" | "description" | null>(null);
};
