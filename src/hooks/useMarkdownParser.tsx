import React from "react";

const useMarkdownParser = () => {
  const parseDescription = (desc: string) => {
    const lines = desc.split("\n");
    const elements: React.ReactNode[] = [];
    let index = 0;

    while (index < lines.length) {
      const line = lines[index];
      const trimmedLine = line.trimStart();
      const indentLevel = line.length - trimmedLine.length;

      if (trimmedLine.startsWith("- ") || trimmedLine.startsWith("+ ")) {
        const [ulItems, newIndex] = parseList(lines, index, indentLevel, "ul");
        elements.push(<ul key={index}>{ulItems}</ul>);
        index = newIndex;
      } else if (/^\d+\. /.test(trimmedLine)) {
        const [olItems, newIndex] = parseList(lines, index, indentLevel, "ol");
        elements.push(<ol key={index}>{olItems}</ol>);
        index = newIndex;
      } else {
        elements.push(<p key={index}>{parseInlineCode(trimmedLine)}</p>);
        index++;
      }
    }

    return elements;
  };

  const parseList = (
    lines: string[],
    startIndex: number,
    currentIndent: number,
    listType: "ul" | "ol"
  ): [React.ReactNode[], number] => {
    const items: React.ReactNode[] = [];
    let index = startIndex;

    while (index < lines.length) {
      const line = lines[index];
      const trimmedLine = line.trimStart();
      const indentLevel = line.length - trimmedLine.length;

      if (indentLevel < currentIndent) break;

      if (
        (listType === "ul" && /^[-+]\s/.test(trimmedLine)) ||
        (listType === "ol" && /^\d+\.\s/.test(trimmedLine))
      ) {
        const contentStartIndex = trimmedLine.search(/\S\s/) + 1;
        const content = parseInlineCode(
          trimmedLine.substring(contentStartIndex)
        );

        const [nestedUlItems, ulNewIndex] = parseList(
          lines,
          index + 1,
          indentLevel + 2,
          "ul"
        );
        const [nestedOlItems, olNewIndex] = parseList(
          lines,
          index + 1,
          indentLevel + 2,
          "ol"
        );

        items.push(
          <li key={index}>
            {content}
            {nestedUlItems.length > 0 && <ul>{nestedUlItems}</ul>}
            {nestedOlItems.length > 0 && <ol>{nestedOlItems}</ol>}
          </li>
        );

        index = Math.max(ulNewIndex, olNewIndex);
      } else {
        index++;
      }
    }

    return [items, index];
  };

  const parseInlineCode = (text: string) => {
    const parts = text.split(/(`[^`]+`)/g);
    return parts.map((part, index) =>
      part.startsWith("`") && part.endsWith("`") ? (
        <code key={index}>{part.slice(1, -1)}</code>
      ) : (
        part
      )
    );
  };

  return { parseDescription, parseInlineCode };
};

export default useMarkdownParser;
