export const COLORS = [
  "#FFD700",
  "#ADFF2F",
  "#7FFFD4",
  "#6495ED",
  "#FF6347",
  "#8A2BE2",
  "#9370DB",
  "#FF69B4",
  "#FFA500",
  "#00CED1",
  "#20B2AA",
  "#DC143C",
  "#8B0000",
  "#00FF7F",
  "#00BFFF",
  "#FF1493",
  "#696969",
  "#7FFF00",
];

export const LAYERS = [
  { title: "1", desc: "- `First Layer`" },
  { title: "2", desc: "- `Second Layer`" },
  { title: "3", desc: "- `Third Layer`" },
  { title: "4", desc: "- `Fourth Layer`" },
  { title: "5", desc: "- `Fifth Layer`" },
  { title: "6", desc: "- `Sixth Layer`" },
  { title: "7", desc: "- `Seventh Layer`" },
  { title: "8", desc: "- `Eighth Layer`" },
  { title: "9", desc: "- `Ninth Layer`" },
  { title: "10", desc: "- `Tenth Layer`" },
  { title: "11", desc: "- `Eleventh Layer`" },
  { title: "12", desc: "- `Twelfth Layer`" },
  { title: "13", desc: "- `Thirteenth Layer`" },
  { title: "14", desc: "- `Fourteenth Layer`" },
  { title: "15", desc: "- `Fifteenth Layer`" },
  { title: "16", desc: "- `Sixteenth Layer`" },
  { title: "17", desc: "- `Seventeenth Layer`" },
  { title: "18", desc: "- `Eighteenth Layer`" },
];

export const INITIAL_COUNT = 7;
export const INITIAL_FONT_SIZE = 16;
export const DEFAULT_TITLE = "Pyramid Visualization";
