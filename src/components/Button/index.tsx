import React from "react";
import type { ButtonProps } from "./types";
import "./styles.css";

const Button: React.FC<ButtonProps> = ({ onClick, text, type = "default" }) => {
  const classNames = `button ${type}`;

  return (
    <button
      className={classNames}
      onClick={onClick}
      disabled={type === "disabled"}
    >
      {text}
    </button>
  );
};

export default Button;
