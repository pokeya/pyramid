type ButtonType = "primary" | "default" | "danger" | "disabled";

export interface ButtonProps {
  onClick: () => void;
  text: string;
  type?: ButtonType;
}
