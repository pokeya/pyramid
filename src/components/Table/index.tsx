import React, { useState, useEffect } from "react";
import {
  type Headers,
  loadTableHeaders,
  saveTableHeaders,
  useEditingHeader,
} from "@/lib/utils";
import useMarkdownParser from "@/hooks/useMarkdownParser";
import type { TableProps } from "./types";
import "./styles.css";

const Table: React.FC<TableProps> = ({ layers, fontSize }) => {
  const { parseDescription, parseInlineCode } = useMarkdownParser();

  const [headers, setHeaders] = useState<Headers>(loadTableHeaders());
  const [editingHeader, setEditingHeader] = useEditingHeader();

  const handleHeaderChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setHeaders((prevHeaders) => {
      const newHeaders = { ...prevHeaders, [name]: value };
      return newHeaders;
    });
  };

  const handleBlur = (name: keyof Headers) => {
    if (!headers[name]) {
      alert("Table header fields cannot be empty!");
    } else {
      saveTableHeaders(headers);
      setEditingHeader(null);
    }
  };

  const handleKeyDown = (e: React.KeyboardEvent, name: keyof Headers) => {
    if (e.key === "Enter") {
      if (!headers[name]) {
        alert("Table header fields cannot be empty!");
      } else {
        saveTableHeaders(headers);
        setEditingHeader(null);
      }
    }
  };

  useEffect(() => {
    const handleBeforeUnload = (event: BeforeUnloadEvent) => {
      if (!headers.title || !headers.description) {
        event.preventDefault();
        event.returnValue = "";
        alert("Table header fields cannot be empty!");
      }
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [headers]);

  return (
    <div className="tables-wrapper">
      <table className="tables" style={{ fontSize: `${fontSize}px` }}>
        <thead>
          <tr>
            <th>
              {editingHeader === "title" ? (
                <input
                  type="text"
                  name="title"
                  style={{ fontSize: `${fontSize}px` }}
                  value={headers.title}
                  onChange={handleHeaderChange}
                  onBlur={() => handleBlur("title")}
                  onKeyDown={(e) => handleKeyDown(e, "title")}
                  autoFocus
                />
              ) : (
                <div className="header-content">
                  {headers.title}
                  <span className="edit-icon-container">
                    <img
                      src="/edit-text.svg"
                      className="edit-icon"
                      style={{ width: `${fontSize}px` }}
                      onClick={() => setEditingHeader("title")}
                    />
                  </span>
                </div>
              )}
            </th>
            <th>
              {editingHeader === "description" ? (
                <input
                  type="text"
                  name="description"
                  style={{ fontSize: `${fontSize}px` }}
                  value={headers.description}
                  onChange={handleHeaderChange}
                  onBlur={() => handleBlur("description")}
                  onKeyDown={(e) => handleKeyDown(e, "description")}
                  autoFocus
                />
              ) : (
                <div className="header-content">
                  {headers.description}
                  <span className="edit-icon-container">
                    <img
                      src="/edit-text.svg"
                      className="edit-icon"
                      style={{ width: `${fontSize}px` }}
                      onClick={() => setEditingHeader("description")}
                    />
                  </span>
                </div>
              )}
            </th>
          </tr>
        </thead>
        <tbody>
          {layers.map((layer, index) => (
            <tr key={index}>
              <td>{parseInlineCode(layer.title || "-")}</td>
              <td>{parseDescription(layer.desc || "None")}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
