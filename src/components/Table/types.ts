export interface TableProps {
  layers: { title: string; desc: string }[];
  fontSize: number;
}
