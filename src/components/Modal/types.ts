export interface ModalProps {
  title: string;
  desc: string;
  setTitle: (title: string) => void;
  setDesc: (desc: string) => void;
  onSave: () => void;
  onCancel: () => void;
}
