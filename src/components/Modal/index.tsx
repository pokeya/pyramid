import React from "react";
import Button from "@/components/Button";
import type { ModalProps } from "./types";
import "./styles.css";

const Modal: React.FC<ModalProps> = ({
  title,
  desc,
  setTitle,
  setDesc,
  onSave,
  onCancel,
}) => {
  const handleBackgroundClick = (
    e: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    if (e.target === e.currentTarget) {
      onCancel();
    }
  };

  const isDisabled = !title && !desc;

  return (
    <div className="modal-background" onClick={handleBackgroundClick}>
      <div className="modal-content">
        <h2>Edit Layer</h2>
        <div className="modal-form">
          <div className="form-group">
            <label htmlFor="title-input">Title:</label>
            <input
              id="title-input"
              type="text"
              value={title}
              placeholder="Enter title"
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="desc-input">Description:</label>
            <textarea
              id="desc-input"
              value={desc}
              placeholder="Enter description"
              onChange={(e) => setDesc(e.target.value)}
            />
          </div>
        </div>
        <div className="modal-buttons">
          <Button onClick={onCancel} text="Cancel" type="default" />
          <Button
            onClick={onSave}
            text="Save"
            type={isDisabled ? "disabled" : "primary"}
          />
        </div>
      </div>
    </div>
  );
};

export default Modal;
