import React from "react";
import type { ToolboxProps } from "./types";
import "./styles.css";

const Toolbox: React.FC<ToolboxProps> = ({
  layerCount,
  setLayerCount,
  initialLayers,
  currentTitle,
  setCurrentTitle,
  setLayers,
  onClose,
  fontSize,
  setFontSize,
}) => {
  const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const chineseChars = value.match(/[\u4e00-\u9fa5]/g) || [];
    const nonChineseChars = value.match(/[^\u4e00-\u9fa5]/g) || [];
    const totalLength = chineseChars.length * 2 + nonChineseChars.length;

    if (totalLength > 45) {
      alert(
        "Title cannot exceed 45 character units (Chinese character counts as 2)."
      );
    } else {
      setCurrentTitle(value);
    }
  };

  const handleLayerCountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    if (value === "") {
      setLayerCount(0);
      setLayers([]);
      return;
    }

    const numValue = parseInt(value, 10);

    if (isNaN(numValue)) {
      alert("Please enter a valid number.");
    } else if (numValue < 0) {
      alert("Number of layers cannot be negative.");
    } else if (numValue > 18) {
      alert("Number of layers cannot exceed 18.");
    } else {
      setLayerCount(numValue);
      setLayers(initialLayers.slice(0, numValue));
    }
  };

  const handleFontSizeChange = (size: number) => {
    setFontSize(size);
  };

  return (
    <div className="toolbox">
      <div className="toolbox-header">
        <div className="toolbox-title">Toolbox</div>
        <button
          className="toolbox-close-button"
          title="close"
          onClick={onClose}
        >
          x
        </button>
      </div>
      <div className="input-container">
        <label htmlFor="title-input">Title:</label>
        <input
          type="text"
          id="title-input"
          value={currentTitle}
          onChange={handleTitleChange}
          placeholder="Enter title"
        />
      </div>
      <div className="input-container">
        <label htmlFor="layerCount">Number of pyramid layers:</label>
        <input
          type="number"
          id="layerCount"
          value={layerCount === 0 ? "0" : layerCount}
          onChange={handleLayerCountChange}
        />
      </div>
      <div className="input-container">
        <label>Font Size:</label>
        <div className="radio-container">
          <div className="radio-options">
            <label>
              <input
                type="radio"
                name="fontSize"
                value="small"
                checked={fontSize === 8}
                onChange={() => handleFontSizeChange(8)}
              />
              Small
            </label>
            <label>
              <input
                type="radio"
                name="fontSize"
                value="medium"
                checked={fontSize === 16}
                onChange={() => handleFontSizeChange(16)}
              />
              Medium
            </label>
            <label>
              <input
                type="radio"
                name="fontSize"
                value="large"
                checked={fontSize === 24}
                onChange={() => handleFontSizeChange(24)}
              />
              Large
            </label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Toolbox;
