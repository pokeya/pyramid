export interface ToolboxProps {
  layerCount: number;
  setLayerCount: (count: number) => void;
  initialLayers: { title: string; desc: string }[];
  currentTitle: string;
  setCurrentTitle: (title: string) => void;
  setLayers: (layers: { title: string; desc: string }[]) => void;
  onClose: () => void;
  fontSize: number;
  setFontSize: (size: number) => void;
}
