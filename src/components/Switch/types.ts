export interface SwitchProps {
  mode: "edit" | "viewer" | "table";
  onToggle: () => void;
}
