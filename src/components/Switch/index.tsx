import React, { useState } from "react";
import type { SwitchProps } from "./types";
import "./styles.css";

const Switch: React.FC<SwitchProps> = ({ mode, onToggle }) => {
  const [isHovered, setIsHovered] = useState(false);
  const [isVisible, setIsVisible] = useState(true);

  if (!isVisible) {
    return null;
  }

  return (
    <div
      className="switch-container"
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <button className={`switch-button ${mode}`} onClick={onToggle}>
        <img
          src={
            mode === "edit"
              ? "/edit.svg"
              : mode === "viewer"
              ? "/readonly.svg"
              : "/table.svg"
          }
          alt={
            mode === "edit"
              ? "Edit Mode"
              : mode === "viewer"
              ? "Viewer Mode"
              : "Table Mode"
          }
          className="switch-icon"
        />
        {mode === "edit" ? "Edit" : mode === "viewer" ? "Viewer" : "Table"}
      </button>
      {isHovered && (
        <button
          className="switch-close-button"
          onClick={() => setIsVisible(false)}
        >
          ×
        </button>
      )}
    </div>
  );
};

export default Switch;
