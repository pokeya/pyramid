export interface LayerProps {
  index: number;
  title: string;
  desc: string;
  color: string;
  baseWidth: number;
  clipPath: string;
  fontSize: number;
  onClick: (index: number) => void;
}
