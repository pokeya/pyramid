import React from "react";
import type { LayerProps } from "./types";
import "./styles.css";

const Layer: React.FC<LayerProps> = ({
  index,
  title,
  desc,
  color,
  baseWidth,
  clipPath,
  fontSize,
  onClick,
}) => {
  return (
    <div
      className="layer"
      style={
        {
          width: `${baseWidth}px`,
          "--layer-color": color,
          "--font-size": `${fontSize}px`,
          clipPath: clipPath,
        } as React.CSSProperties
      }
      title={desc}
      data-desc={desc}
      onClick={() => onClick(index)}
    >
      <span style={{ fontSize: `var(--font-size)` }}>{title}</span>
    </div>
  );
};

export default Layer;
