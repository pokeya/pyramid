import React, { useState } from "react";
import Layer from "@/components/Layer";
import Modal from "@/components/Modal";
import Toolbox from "@/components/Toolbox";
import Switch from "@/components/Switch";
import Table from "@/components/Table";
import type { PyramidProps } from "./types";
import "./styles.css";

const Pyramid: React.FC<PyramidProps> = ({
  colors,
  layers: initialLayers,
  initialCount,
  title: initialTitle,
  fontSize: initialFontSize,
}) => {
  const [layerCount, setLayerCount] = useState<number>(initialCount);
  const [layers, setLayers] = useState(initialLayers.slice(0, layerCount));
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentLayerIndex, setCurrentLayerIndex] = useState<number | null>(
    null
  );
  const [currentTitle, setCurrentTitle] = useState("");
  const [currentDesc, setCurrentDesc] = useState("");
  const [toolboxTitle, setToolboxTitle] = useState(initialTitle);
  const [isToolboxOpen, setIsToolboxOpen] = useState(true);
  const [mode, setMode] = useState<"edit" | "viewer" | "table">("edit");
  const [fontSize, setFontSize] = useState<number>(initialFontSize);

  const handleLayerClick = (index: number) => {
    if (mode === "edit") {
      setCurrentLayerIndex(index);
      setCurrentTitle(layers[index].title);
      setCurrentDesc(layers[index].desc);
      setIsModalOpen(true);
    }
  };

  const handleSave = () => {
    if (currentLayerIndex !== null) {
      const updatedLayers = [...layers];
      updatedLayers[currentLayerIndex] = {
        ...updatedLayers[currentLayerIndex],
        title: currentTitle,
        desc: currentDesc,
      };
      setLayers(updatedLayers);
    }
    setIsModalOpen(false);
  };

  const calcClipPath = (index: number) => {
    const totalLayers = layers.length + 1;
    const topWidth = (index / (totalLayers - 1)) * 100;
    const bottomWidth = ((index + 1) / (totalLayers - 1)) * 100;
    return `polygon(${50 - topWidth / 2}% 0%, ${50 + topWidth / 2}% 0%, ${
      50 + bottomWidth / 2
    }% 100%, ${50 - bottomWidth / 2}% 100%)`;
  };

  const baseWidth = 200 + (layers.length - 1) * 100;

  const handleToggleMode = () => {
    setMode((prevMode) => {
      if (prevMode === "edit") {
        return "viewer";
      } else if (prevMode === "viewer") {
        return "table";
      } else {
        setIsToolboxOpen(true);
        return "edit";
      }
    });
  };

  return (
    <div className="container">
      <h1>{toolboxTitle}</h1>
      {isToolboxOpen && mode === "edit" && (
        <Toolbox
          layerCount={layerCount}
          setLayerCount={setLayerCount}
          initialLayers={initialLayers}
          currentTitle={toolboxTitle}
          setCurrentTitle={setToolboxTitle}
          setLayers={setLayers}
          onClose={() => {
            setIsToolboxOpen(false);
            setMode("viewer");
          }}
          fontSize={fontSize}
          setFontSize={setFontSize}
        />
      )}
      <div className={`content ${mode === "table" ? "table-mode" : ""}`}>
        <div className="pyramid">
          {layers.map((layer, index) => (
            <Layer
              key={index}
              index={index}
              title={layer.title}
              desc={layer.desc}
              color={colors[index]}
              baseWidth={baseWidth}
              clipPath={calcClipPath(index)}
              onClick={handleLayerClick}
              fontSize={fontSize}
            />
          ))}
        </div>
        {mode === "table" && layers.length > 0 && (
          <Table layers={layers} fontSize={fontSize} />
        )}
      </div>
      {isModalOpen && (
        <Modal
          title={currentTitle}
          desc={currentDesc}
          setTitle={setCurrentTitle}
          setDesc={setCurrentDesc}
          onSave={handleSave}
          onCancel={() => setIsModalOpen(false)}
        />
      )}
      <Switch mode={mode} onToggle={handleToggleMode} />
    </div>
  );
};

export default Pyramid;
