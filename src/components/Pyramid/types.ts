export interface PyramidProps {
  colors: string[];
  layers: { title: string; desc: string }[];
  initialCount: number;
  title: string;
  fontSize: number;
}
