import React from "react";
import Pyramid from "@/components/Pyramid";
import {
  COLORS,
  LAYERS,
  INITIAL_COUNT,
  INITIAL_FONT_SIZE,
  DEFAULT_TITLE,
} from "@/constants";
import "./index.css";

const App: React.FC = () => {
  return (
    <div className="app">
      <Pyramid
        colors={COLORS}
        layers={LAYERS}
        initialCount={INITIAL_COUNT}
        fontSize={INITIAL_FONT_SIZE}
        title={DEFAULT_TITLE}
      />
    </div>
  );
};

export default App;
