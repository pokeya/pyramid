# Pyramid

This project is a simple React application that displays a pyramid structure with customizable layers. Users can edit the title and description of each layer through a modal dialog.

[查看中文文档](./README_zh.md)

## Features

- Display a pyramid with customizable layers
- Edit layer title and description through a modal
- Dynamic input for the number of layers
- Dynamically adjust the text content size
- Switch between edit, view, and table modes
- Modern and clean UI design
- Experimental: Support partial `md` format for description information

## Examples

![English Version](/public/English-Version.png)

## Live

https://pyramid-zeta.vercel.app/

## License

This project is licensed under the MIT License.
