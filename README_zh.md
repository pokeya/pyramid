# Pyramid

这个项目是一个简单的 React 应用程序，显示一个具有可定制层的金字塔结构。用户可以通过模态对话框编辑每一层的标题和描述。

[View in English](./README.md)

## 功能

- 显示具有可定制层的金字塔
- 通过模态编辑层的标题和描述
- 动态输入金字塔层数
- 动态调整文本内容大小
- 可切换编辑、查看和表格模式
- 现代和简洁的用户界面设计
- 实验性：支持部分 `md` 格式的描述信息

## 示例

![Chinese Version](/public/Chinese-Version.png)

## 在线演示

https://pyramid-zeta.vercel.app/

## 许可证

该项目采用 MIT 许可证。
